﻿function CustomTable(TableId, Columns) {
    try {
        this.Table = $("#" + TableId);
        this.TableId = TableId;
        this.Columns = Columns;
    } catch (e) { alert(e); }
}

CustomTable.prototype = {
    constructor: CustomTable,
    setData: function (data) {
        try {
            this.JsonDatas = data;
        } catch (e) { alert(e); }
    },
    refresh: function () {
        try {
            this.empty();
            var table = document.getElementById(this.TableId);
            for (i = 0; i < this.JsonDatas.length; i++) {
                var row = document.createElement("tr");
                var cell = document.createElement("td");
                cell.innerHTML = i + 1;
                row.append(cell);

                for (j = 0; j < this.Columns.length; j++) {
                    cell = document.createElement("td");
                    cell.innerHTML = this.Columns[j].render(this.JsonDatas[i][this.Columns[j].data], this.JsonDatas[i], this.Columns[j].data);
                    row.append(cell);
                }
                $("#" + this.TableId).children('tbody').append(row);
            }
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        } catch (e) { alert(e); }
    },
    empty: function () {
        try {
            $("#" + this.TableId).children('tbody').empty();
        } catch (e) { alert(e); }
    },
    addedData: function (data) {
        this.JsonDatas.push(data);
        this.refresh();
    }
}